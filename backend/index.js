const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const setupRoutes = require("./routes.js");
const initDb = require("./db");

app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));

initDb()
  .then(db => {
    const server = setupRoutes(app, db).listen(3001, function() {
      console.log("app running on port.", server.address().port);
    });
  })
  .catch(err => {
    console.error("Failed to make all database connections!");
    console.error(err);
    process.exit(1);
  });
