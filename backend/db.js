const MongoClient = require("mongodb").MongoClient;

require("dotenv").config();

const user = encodeURIComponent(process.env.DB_USER);
const password = encodeURIComponent(process.env.DB_PASS);

const url = `mongodb://${user}:${password}@${
  process.env.DB_HOST
}:27017/admin?authMechanism=SCRAM-SHA-1&authSource=admin`;

module.exports = () =>
  MongoClient.connect(
    url,
    {
      useNewUrlParser: true
    }
  ).then(client => client.db(process.env.DB_NAME));
