const findTenants = function(db) {
  const collection = db.collection("tenant");
  return collection.find({}).toArray();
};

const findTeams = (db, tenantId) => {
  return db
    .collection("team")
    .find({
      "tenantId.identifier": tenantId
    })
    .toArray();
};

const findTeamActivities = async (db, teamId) => {
  const teamDOA = await db
    .collection("domainObjectActivities")
    .findOne({ "domainId.identifier": teamId });

  const ids = teamDOA.activityDefinitionIds.map(id => id.identifier);

  const activityDefinitions = await db
    .collection("activityDefinition")
    .find({ "activityDefinitionId.identifier": { $in: ids } })
    .toArray();

  const activityDefinition = activityDefinitions.reduce(
    (acc, item) =>
      Object.assign(acc, {
        [item.eventCode]: Object.assign(item, {
          properties: item.properties.reduce(
            (acc2, p) => Object.assign(acc2, { [p.key]: p }),
            {}
          )
        })
      }),
    {}
  );

  const logEvents = await db
    .collection("logEvent")
    .aggregate([
      {
        $match: {
          "type.key": "ACTIVITY_DEFINITION",
          "properties.value": { $in: ids }
        }
      },
      {
        $lookup: {
          from: "user",
          localField: "userId.identifier",
          foreignField: "userId.identifier",
          as: "user"
        }
      },
      {
        $unwind: "$user"
      },
      {
        $project: { user: "$user.name", properties: 1, timestamp: 1 }
      }
    ])
    .toArray();

  const transformedEvents = logEvents.map(ev => {
    const activityDefId = ev.properties.find(p => p.key === "code").value;
    const activityDef = activityDefinition[activityDefId];
    const base = {
      name: activityDef.name,
      description: activityDef.description,
      user: ev.user,
      timestamp: ev.timestamp,
      id: ev._id
    };

    base.props = ev.properties.reduce(
      (acc, p) =>
        p.key in activityDef.properties
          ? acc.concat([
              { name: activityDef.properties[p.key].name, value: p.value }
            ])
          : acc,
      []
    );

    return base;
  });

  return { logEvents: transformedEvents, activityDefinitions };
};

module.exports = (app, db) => {
  app.get("/", (req, res) => {
    res.status(200).send("Welcome to our restful API");
  });

  app.get("/tenants", async (req, res) => {
    const tenantsRaw = await findTenants(db);
    const tenants = tenantsRaw.map(t => ({
      label: t.name,
      value: t.tenantId.identifier
    }));
    console.log(`Sending ${tenants.length} tenants`);

    res.send(tenants);
  });

  app.get("/team/:tenantId", async (req, res) => {
    const { tenantId } = req.params;
    const teamsRaw = await findTeams(db, tenantId);
    const teams = teamsRaw.map(t => ({
      label: t.name,
      value: t.teamId.identifier
    }));

    console.log(`Sending ${teams.length} ${tenantId} teams`);

    res.send(teams);
  });

  app.get("/activities/:teamId", async (req, res) => {
    const { teamId } = req.params;
    console.time("Fetch team activities");
    const activities = await findTeamActivities(db, teamId);
    console.timeEnd("Fetch team activities");
    res.send(activities);
  });

  return app;
};
