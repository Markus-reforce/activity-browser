import React, { Component } from "react";
import Select from "react-select";
import "./App.css";
import logo from "./logo.svg";

const API_BASE = "http://localhost:3001";

const fetcher = route =>
  fetch(API_BASE + route, { mode: "cors" }).then(res => res.json());

const formatDate = dateStr =>
  dateStr
    .split("T")
    .join(" ")
    .slice(0, -4);

function handleSelect(currentType, nextType, nextUrl, selected) {
  this.setState(
    state => ({
      [currentType]: { ...state[currentType], selected },
      [nextType]: getInitial({ isLoading: true }),
      term: getInitial()
    }),
    () => {
      fetcher(nextUrl + selected.value)
        .then(data =>
          this.setState(
            {
              [nextType]: {
                ...getInitial(),
                data
              }
            },
            () => this[nextType] && this[nextType].focus()
          )
        )
        .catch(e => alert(`Failed to fetch ${nextType}s:` + e.toString()));
    }
  );
}

const getInitial = (override = {}) => ({
  data: [],
  isLoading: false,
  selected: null,
  ...override
});

class App extends Component {
  state = {
    tenant: getInitial({ isLoading: true }),
    team: getInitial(),
    activities: getInitial(),
    activityFilter: null
  };

  componentDidMount() {
    fetcher("/tenants")
      .then(data => {
        this.setState({
          tenant: { data, isLoading: false, selected: null }
        });
      })
      .catch(e => alert(e.toString()));
  }

  handleTenantSelect = handleSelect.bind(this, "tenant", "team", "/team/");

  handleTeamSelect = handleSelect.bind(
    this,
    "team",
    "activities",
    "/activities/"
  );

  render() {
    const { tenant, team, activities, activityFilter } = this.state;
    console.log(activities);

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to Cache Buster</h1>
        </header>
        <div className="main">
          <p className="App-intro">To get started, select a Tenant</p>
          <Select
            className="mb"
            options={tenant.data}
            ref={ref => {
              this.tenant = ref;
            }}
            value={tenant.selected}
            onChange={this.handleTenantSelect}
            placeholder={
              tenant.isLoading ? "Loading Tenants..." : "Select Tenant"
            }
          />
          {tenant.selected && (
            <Select
              className="mb"
              options={team.data}
              ref={ref => {
                this.team = ref;
              }}
              value={team.selected}
              onChange={this.handleTeamSelect}
              placeholder={team.isLoading ? "Loading Teams..." : "Select Team"}
            />
          )}
          {team.selected &&
            !activities.isLoading && (
              <div>
                <Select
                  className="mb"
                  options={activities.data.activityDefinitions.map(ad => ({
                    label: ad.name,
                    value: ad
                  }))}
                  value={activityFilter}
                  placeholder="Filter on activity"
                />
                <table>
                  <thead />
                  <tbody>
                    {activities.data.logEvents.map(a => (
                      <tr>
                        <td title={a.description}>{a.name}</td>
                        <td>{a.user}</td>
                        <td>{formatDate(a.timestamp)}</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            )}
        </div>
      </div>
    );
  }
}

export default App;
